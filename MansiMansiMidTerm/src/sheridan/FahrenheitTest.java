package sheridan;



import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class FahrenheitTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testRegular() {
		assertTrue("Passed", Fahrenheit.convertFromCelsius(50) == 122);
	}
	
	@Test
	public void testRegular2() {
		assertTrue("Passed", Fahrenheit.convertFromCelsius(0) == 32);
	}
	@Test
	public void testRegular3() {
		assertTrue("Passed", Fahrenheit.convertFromCelsius(60) == 140);
	}
	
	@Test
	public void testIsNegtiveValue() {
		assertTrue("InValid Input", Fahrenheit.convertFromCelsius(-10) == 14);
	}	
	@Test
	public void testIsBoundaryIn() {
		assertTrue("passed", Fahrenheit.convertFromCelsius(0) == 32);
	}

	@Test
	public void testIsBoundaryOut() {
		assertTrue("InValid input", Fahrenheit.convertFromCelsius(-1) == 31);
	}

}






